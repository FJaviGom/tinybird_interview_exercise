# Dashboard Tinybird Exercise

The purpose of this dashboard is to show which of the pickup locations are the most used in a desired interval of time.

It could be useful for taxi drivers who might be interested in the most used pickup locations and also for users who could know what are the locations to avoid if they are in a hurry or the main pickup locations.

## How to
### Install dependencies

To install the needed dependencies, it should run the command:

```bash
npm install
```

### Start a development server

Once all the dependencies are installed, it could run the command:

```bash
npm run start
```

### Build the application

It is possible to build the application using the following command:

```bash
npm run build
```

## Aspects to take into account:
1. No frameworks have been used.
2. It has been supposed that the users know the ID of the pickup locations. If not, that could be translated into more friendly data in a future iteration.
3. Fewer dependencies the best. I have tried to keep it the most simple I can, without introducing any style preprocessor, complex structure or any external library which could help in the development of the dashboard.
4. The time input does not work correctly in Safari or Internet Explorer for now. It is treated as text input. That's why a pattern attribute is added to those inputs.
