import tinybird from 'tinybird.js'
import './styles.css'

const app = (parentNodeId) => {
  const header = document.createElement('h2')
  const headerTextNode = document.createTextNode('Top pickup locations 2017')
  header.appendChild(headerTextNode)

  const formElement = document.createElement('form')
  formElement.addEventListener('submit', (e) => {
    e.preventDefault()

    const formData = new FormData(e.target)

    setWidget(
      parentNodeId,
      formData.get('fromTime') || undefined,
      formData.get('toTime') || undefined
    )
    console.log(Array.from(formData))
  })

  const fromDivElement = document.createElement('div')
  fromDivElement.setAttribute('class', 'input')

  const fromLabelElement = document.createElement('label')
  fromLabelElement.setAttribute('for', 'fromTime')
  fromLabelElement.innerHTML = 'Initial Time:'
  fromDivElement.appendChild(fromLabelElement)

  const fromTimeElement = document.createElement('input')
  fromTimeElement.setAttribute('type', 'time')
  fromTimeElement.setAttribute('id', 'fromTime')
  fromTimeElement.setAttribute('name', 'fromTime')
  fromTimeElement.setAttribute('pattern', '[0-9]{2}:[0-9]{2}')
  fromTimeElement.setAttribute('value', '00:00')
  fromDivElement.appendChild(fromTimeElement)

  formElement.appendChild(fromDivElement)

  const toDivElement = document.createElement('div')
  toDivElement.setAttribute('class', 'input')

  const toLabelElement = document.createElement('label')
  toLabelElement.setAttribute('for', 'toTime')
  toLabelElement.innerHTML = 'Final Time:'
  toDivElement.appendChild(toLabelElement)

  const toTimeElement = document.createElement('input')
  toTimeElement.setAttribute('type', 'time')
  toTimeElement.setAttribute('id', 'toTime')
  toTimeElement.setAttribute('name', 'toTime')
  toTimeElement.setAttribute('pattern', '[0-9]{2}:[0-9]{2}')
  toTimeElement.setAttribute('value', '23:59')
  toDivElement.appendChild(toTimeElement)

  formElement.appendChild(toDivElement)

  const submitElement = document.createElement('input')
  submitElement.setAttribute('id', 'submit-btn')
  submitElement.setAttribute('type', 'submit')
  submitElement.setAttribute('disabled', true)

  formElement.appendChild(submitElement)

  document.getElementById(parentNodeId).append(header, formElement)

  setWidget(parentNodeId)
}

const setWidget = (parentNodeId, fromTime = '00:00', toTime = '23:59') => {
  document.getElementById('top-pickups-table')?.remove()

  const loadingNode = document.createTextNode('Loading...')

  document.getElementById(parentNodeId).append(loadingNode)
  document.getElementById('submit-btn').setAttribute('disabled', true)

  let tinyb = tinybird(
    'p.eyJ1IjogIjdmOTIwMmMzLWM1ZjctNDU4Ni1hZDUxLTdmYzUzNTRlMTk5YSIsICJpZCI6ICJmZTRkNWFiZS05ZWIyLTRjMjYtYWZiZi0yYTdlMWJlNDQzOWEifQ.P67MfoqTixyasaMGH5RIjCrGc0bUKvBoKMwYjfqQN8c'
  )
  const pipe = tinyb.pipe('yellow_tripdata_2017_pipe')

  const [fromHour, fromMinute] = fromTime.split(':')
  const [toHour, toMinute] = toTime.split(':')

  pipe
    .json({
      q: `
      SELECT count() count, pulocationid
      FROM _
      WHERE toHour(tpep_pickup_datetime) >= ${fromHour}
      AND toMinute(tpep_pickup_datetime) >= ${fromMinute}
      AND toHour(tpep_pickup_datetime) <= ${toHour}
      AND toMinute(tpep_pickup_datetime) <= ${toMinute}
      GROUP BY pulocationid
      ORDER BY count() DESC
      LIMIT 10
      `,
    })
    .finally(() => {
      loadingNode.remove()
      document.getElementById('submit-btn').removeAttribute('disabled')
    })
    .then((res) => {
      if (res.error) {
        console.error(res.error)
      } else {
        const table = document.createElement('table')
        table.setAttribute('id', 'top-pickups-table')

        const headerRow = document.createElement('tr')

        const firstHeaderElement = document.createElement('th')
        firstHeaderElement.appendChild(document.createTextNode('position'))
        headerRow.appendChild(firstHeaderElement)

        const secondHeaderElement = document.createElement('th')
        secondHeaderElement.appendChild(
          document.createTextNode('pick-up location ID')
        )
        headerRow.appendChild(secondHeaderElement)

        const thirdHeaderElement = document.createElement('th')
        thirdHeaderElement.appendChild(document.createTextNode('trip count'))
        headerRow.appendChild(thirdHeaderElement)

        table.appendChild(headerRow)

        res.data.forEach((data, index) => {
          const row = document.createElement('tr')

          const firstData = document.createElement('td')
          const firstDataContentNode = document.createTextNode(index + 1)
          firstData.appendChild(firstDataContentNode)
          row.appendChild(firstData)

          const secondData = document.createElement('td')
          const secondDataContentNode = document.createTextNode(
            data.pulocationid
          )
          secondData.appendChild(secondDataContentNode)
          row.appendChild(secondData)

          const thirdData = document.createElement('td')
          const thirdDataContentNode = document.createTextNode(data.count)
          thirdData.appendChild(thirdDataContentNode)
          row.appendChild(thirdData)

          table.appendChild(row)
        })

        document.getElementById(parentNodeId).append(table)
      }
    })
}

app('dashboard')
